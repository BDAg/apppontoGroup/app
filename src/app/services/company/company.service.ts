import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Storage } from '@ionic/storage';
import { WarningService } from '../warning/warning.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  pathUrl = 'empresas';

  constructor(
    private apiService: ApiService,
    private storage: Storage,
    private warningService: WarningService
  ) { }

  getCompaniesApi() {
    this.apiService.get(`${this.pathUrl}`)
      .subscribe((companies: any) => {
        this.setCompanies(JSON.stringify(companies.empresas));
      }, (err) => {
        this.warningService.handleError('Erro ao carregar empresas.', err);
      });
  }

  async post(company) {
    return await new Promise((resolve, reject) => {
      this.apiService.post(`${this.pathUrl}`, company)
        .subscribe((data: any) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  async getCompanies() {
    return await this.storage.get('companies');
  }

  async setCompanies(companies: string) {
    return await this.storage.set('companies', companies);
  }
}
