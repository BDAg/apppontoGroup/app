import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Storage } from '@ionic/storage';
import { WarningService } from '../warning/warning.service';

@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  pathUrl = 'qrcodes';

  constructor(
    private apiService: ApiService,
    private storage: Storage,
    private warningService: WarningService
  ) { }

  async post(code: string, lat: number, long: number, idCompany: number) {
    return await new Promise((resolve, reject) => {
      this.apiService.post(`${this.pathUrl}`, {
        qrcode: code,
        latitude: lat,
        longitude: long,
        id_empresa: idCompany
      })
        .subscribe((data: any) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  getQrcodesApi() {
    this.apiService.get(`${this.pathUrl}`)
      .subscribe((qrcodes: any) => {
        this.setQrcodes(JSON.stringify(qrcodes.qrcodes));
      }, (err) => {
        this.warningService.handleError('Erro ao carregar empresas.', err);
      });
  }

  async getQrcodes() {
    return await this.storage.get('qrcodes');
  }

  async setQrcodes(qrcodes) {
    return await this.storage.set('qrcodes', qrcodes);
  }
}
