import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancamentosNovoPage } from './lancamentos-novo.page';

describe('LancamentosNovoPage', () => {
  let component: LancamentosNovoPage;
  let fixture: ComponentFixture<LancamentosNovoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancamentosNovoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancamentosNovoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
