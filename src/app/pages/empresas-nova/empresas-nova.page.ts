import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { WarningService } from 'src/app/services/warning/warning.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { Empresa } from 'src/app/models/empresa.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empresas-nova',
  templateUrl: './empresas-nova.page.html',
  styleUrls: ['./empresas-nova.page.scss'],
})
export class EmpresasNovaPage implements OnInit {

  company: Empresa = {
    nome: '',
    latitude: '',
    longitude: '',
  };

  btnEnabled: any;

  constructor(
    private geolocation: Geolocation,
    private warningService: WarningService,
    private companyService: CompanyService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.geolocation.getCurrentPosition()
      .then((data: any) => {
        this.company.latitude = data.coords.latitude;
        this.company.longitude = data.coords.longitude;
      })
      .catch((err: any) => {
        this.warningService.showToast('Erro ao carregar posição GPS.');
        console.log('Erro ao carregar posição GPS: ', err);
      });

    this.btnEnabled = true;
  }

  sendCompany() {
    this.btnEnabled = false;
    this.companyService.post(this.company)
      .then((data: any) => {
        this.btnEnabled = true;
        this.warningService.showToast('Empresa salva com sucesso.');
        this.router.navigateByUrl('lancamentos');
      })
      .catch((err: any) => {
        this.btnEnabled = true;
        this.warningService.showToast('Erro ao salvar empresa.');
        console.log('Erro ao salvar empresa: ', err);
      });
  }

}
