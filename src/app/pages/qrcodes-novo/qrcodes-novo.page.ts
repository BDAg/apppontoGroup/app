import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/services/company/company.service';
import { QrcodeService } from 'src/app/services/qrcode/qrcode.service';
import { WarningService } from 'src/app/services/warning/warning.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qrcodes-novo',
  templateUrl: './qrcodes-novo.page.html',
  styleUrls: ['./qrcodes-novo.page.scss'],
})
export class QrcodesNovoPage implements OnInit {

  btnEnabled: any;

  companies: any;
  company: any;
  qrcode: any;
  latitude: any;
  longitude: any;

  constructor(
    private scanner: BarcodeScanner,
    private warningService: WarningService,
    private companyService: CompanyService,
    private qrcodeService: QrcodeService,
    private geolocation: Geolocation,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.geolocation.getCurrentPosition()
      .then((data: any) => {
        this.latitude = data.coords.latitude;
        this.longitude = data.coords.longitude;
      })
      .catch((err: any) => {
        this.warningService.showToast('Erro ao carregar posição GPS.');
        console.log('Erro ao carregar posição GPS: ', err);
      });

    this.companyService.getCompanies()
      .then((companies) => {
        this.companies = JSON.parse(companies);
      })
      .catch((err) => {
        this.warningService.showToast('Erro ao carregar empresas.');
        console.log('Erro ao carregar empresas: ', err);
      });

    this.btnEnabled = true;
  }

  qrCodeReader() {
    this.scanner.scan()
      .then((data: any) => {
        this.qrcode = data.text;
      })
      .catch((err: any) => {
        this.warningService.showToast('Erro ao ler QRCode.');
        console.log('Erro ao ler QRCode: ', err);
      });
  }

  sendQrcode() {
    this.btnEnabled = false;
    this.qrcodeService.post(this.qrcode, this.latitude, this.longitude, this.company)
      .then((data: any) => {
        this.btnEnabled = true;
        this.warningService.showToast('QRCode salvo com sucesso.');
        this.router.navigateByUrl('lancamentos');
      })
      .catch((err: any) => {
        this.btnEnabled = true;
        this.warningService.showToast('Erro ao salvar QRCode.');
        console.log('Erro ao salvar QRCode: ', err);
      });
  }

}
