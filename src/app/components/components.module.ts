import { NgModule } from '@angular/core';
import { ResultadoVazioComponent } from './resultado-vazio/resultado-vazio.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ResultadoVazioComponent
    ],
    imports: [
        IonicModule.forRoot(),
        CommonModule
    ],
    exports: [
        ResultadoVazioComponent
    ]
})
export class ComponentsModule { }
